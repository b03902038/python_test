#!/usr/bin/env python3
from member import *

def add_member(input_list, name_dict):
    if len(input_list) < 4:
        print("wrong input")
        return
    if input_list[1] == "teacher":
        name_dict[input_list[2]] = teacher.teacher(input_list[2], int(input_list[3]))
    elif input_list[1] == "student":
        name_dict[input_list[2]] = student.student(input_list[2], int(input_list[3]))
    elif input_list[1] == "special_student":
        name_dict[input_list[2]] = special_student.special_student(input_list[2], int(input_list[3]))
    else:
        print("wrong member")

if __name__ == "__main__":
    name_dict = {}
    while True:
        try:
            input_string = input()
            split_list = input_string.split(" ")

            if split_list[0] == 'add':
                add_member(split_list, name_dict)

            elif split_list[0] == 'remove':
                if name_dict.pop(split_list[1], "not found") == "not found":
                    print("no this person")

            elif split_list[0] == 'greet':
                if name_dict.get(split_list[1], "not found") == "not found":
                    print("no this person")
                else:
                    name_dict[split_list[1]].greet()

            elif split_list[0] == 'show':
                all_members = name_dict.values()
                for m in all_members:
                    m.show()
            else:
                print("wrong input")

        except EOFError:
            break 
