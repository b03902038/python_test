#!/usr/bin/env python3
from member.student import student

class special_student(student):
    cls_name = "special_student"

    def __init__(self, init_name, init_score):
        self.name = init_name
        self.score = int(init_score ** 0.5 * 10)
