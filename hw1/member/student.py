#!/usr/bin/env python3
from member.base_member import base_member

class student(base_member):
    cls_name = "student"

    def __init__(self, init_name, init_score):
        self.name = init_name
        self.score = init_score

    def __str__(self):
        return "Name: " +  self.name + " Score: " + str(self.score)

