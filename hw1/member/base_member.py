#!/usr/bin/env python3

class base_member:
    cls_name = "base_member"

    def __init__(self, init_name):
        self.name = init_name

    @classmethod
    def greet(cls):
        print("Hi, I am a", cls.cls_name)

    def show(self):
        print(self.__str__())
