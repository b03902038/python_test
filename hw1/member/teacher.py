#!/usr/bin/env python3
from member.base_member import base_member

class teacher(base_member):
    cls_name = "teacher"

    def __init__(self, init_name, init_age):
        self.name = init_name
        self.age = init_age

    def __str__(self):
        return "Name: " +  self.name + " Age: " + str(self.age)
